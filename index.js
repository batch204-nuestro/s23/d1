// create an object

//Object Literal
	/*
		let = {
			keyA: vallue,
			keyB: valueB
		}
	*/

let cellphone = {
	name: "nokia 3210",
	manufactureDate: 1999
}

console.log(cellphone);
console.log(typeof cellphone);

function Laptop(name, manufacturerDate) {
	this.name = name;
	this.manufactureDate = manufacturerDate;
}

let laptop = new Laptop('lenovo', 2008);
console.log(laptop);

let oldLaptop = Laptop('ibm', 1980);
console.log(oldLaptop);

let myLaptop = new Laptop ('mac', '2020')
console.log(myLaptop);

//Create Empty objects
let computer = {};
console.log(computer);

//how do we access object properties?
//Dot notation

console.log(myLaptop.name);
console.log(myLaptop.manufactureDate);

//Square bracket notation
console.log(myLaptop['name'])

let array = [laptop,myLaptop];
console.log(array);

console.log(array[1].name)
console.log(array[1]['name'])
//Initializing /Add / Delete / Reassigning Object properties

let car = {};
console.log(car);

car.name = 'sarao';
console.log(car);
car['manufacture date'] = '2019'
console.log(car);

//Deleting objects properties
delete car['manufacture date'];
console.log(car);

//Re assigning objects properties
car.name = 'mustang'
console.log(car)

//Object Methods

let person = {
name: 'jack',
talk: function () {
	console.log('hello! my name is ' + this.name);
}

}

console.log(person);
person.talk();

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward')
}

person.walk();


let myPokemon = {

	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,

	tackle: function() {
		console.log(this.name + ' tackle enemy')
		console.log("targetPokemon's health is low")
	},

	faint: function() {
		console.log('pokemon fainted.')
	}
}

console.log(myPokemon);

function Pokemon(name, level, health) {

	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s is now reduced to " + (target.health - this.attack))
		target.health = (target.health - this.attack);
				if (target.health <= 5) {
				target.faint();
			}

	},

	this.faint = function() {
		console.log(this.name + " fainted")
	}

}

let squirtle = new Pokemon ('squirtle', 100, 200);
let snorlax = new Pokemon ('snorlax', 90, 150)

snorlax.tackle(squirtle);
snorlax.tackle(squirtle);
snorlax.tackle(squirtle);